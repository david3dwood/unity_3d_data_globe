# Unity_interactive_Globe_3D_viewer_i-3G

Unity Globe data visualizer.   I got it to load a custom JHU covid data set and got it to switch between and unload other datasets but I need a better world dataset. I have US and per country data.  Need to enable real-time filters.

Summary: 

base code by Abdullah Aldandarawy
data implementation and page design,  David Wood 4/10/20 

Unity Globe is a complete project for visualizing latitude longitude based information. You can simply extend it by adding your own data to create your globe.   This project is a Unity3D implementation for WebGL-Globe created by Google Data Arts Team.

This implementation of Covid-19 US data is a discrete selection of dates as of 4/10/20.   This demo is for experimental and development purposes only, not to be used for analytics other than a 3d representation of values.   NOTE:  This is portraying the LOG value of the and not the raw total numerical values. -  djw 4/10/20

Data source 2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE - Johns Hopkins University Center for Systems Science and Engineering (JHU CSSE). Johns Hopkins University Applied Physics Lab (JHU APL).

references:   
https://assetstore.unity.com/packages/templates/systems/globe-data-visualizer-80008
https://github.com/Dandarawy/Unity3D-Globe     
https://github.com/CSSEGISandData/COVID-19


![Test Image 6](screencap2.PNG)